<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">FLORENCIA</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-9 caracteristicas especs">
                <div class="col-md-6 caracteristicas" style="padding: 0;margin-top: 0;">
                    <h4>GALERÍA DE IMÁGENES</h4>
                    <hr>
                    <br>
                    <br>                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid gallery">
    <div class="container ">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal1.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal2.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal3.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal4.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal5.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal6.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal7.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal8.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="/img/products/<?= $tipo ?>gal9.jpg" alt=""/>
            </div>
        </div>
    </div>
</div>
