<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">FORTE</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="/img/products/product_forte.jpg" alt="" class="fillwide"/>
                <br>
                <br>
            </div>
            <div class="col-md-6 uglygrey">
                <div>
                    Su diseño de aberturas prácticamente impenetrables es imposible de escalar, esto la convierte en la mejor solución para áreas de alta seguridad.  
                </div>
            </div>
            <div class="col-md-6" style="text-align: right">
                <img src="/img/products/icon_milan_2.png" alt="" style="margin-left: 40px;"/>
                <img src="/img/products/icon_milan_1.png" alt="" style="margin-left: 40px;"/>
            </div>
        </div>
    </div>
    <div class="container caracteristicas">
        <div class="row">
            <div class="col-sm-6">
                <h4>Características</h4>
                <hr>
                <article>
                    <ul>
                        <li>
                            <strong>Difícil de escalar</strong>
                            <p>Las aberturas de la Reja Deacero Forte son sumamente cerradas, lo que la convierte en un sistema antiescalable.</p>
                        </li>
                        <li>
                            <strong>Resistencia estructural</strong>
                            <p>Componentes diseñados para una alta resistencia.</p>
                        </li>
                        <li>
                            <strong>Versátil</strong>
                            <p>Dos diseños disponibles: panel plano o con pliegues.</p>
                        </li>
                        <li>
                            <strong>Impenetrable</strong>
                            <p>Su diseño dificulta el poder cortar sus alambres con pinzas o cizallas.</p>
                        </li>
                    </ul>
                </article>
            </div>
            <div class="col-sm-6">
                <h4>Aplicaciones</h4>
                <hr>
                <article>
                    <ul>
                        <li>
                            Aeropuertos.
                        </li>
                        <li>
                            Instalaciones de gobierno.
                        </li>
                        <li>
                            Ferrocarriles.
                        </li>
                        <li>
                            Zonas militares.
                        </li>
                        <li>
                            Estaciones eléctricas.
                        </li>
                        <li>
                            Zoológicos.
                        </li>
                        <li>
                            Prisiones.
                        </li>
                        <li>
                            Otras.
                        </li>
                    </ul>
                </article>
            </div>
        </div>
    </div>
</div>
