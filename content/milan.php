<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">MILÁN</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="/img/products/product_milan.jpg" alt="" class="fillwide"/>
                <br>
                <br>
            </div>
            <div class="col-md-6 uglygrey">
                <div>
                    Está fabricada con alambre continuo que le permite tener acabados curvos en su parte superior e inferior y secciones rectangulares en la parte central.                
                </div>
            </div>
            <div class="col-md-6" style="text-align: right">
                <img src="/img/products/icon_milan_2.png" alt="" style="margin-left: 40px;"/>
                <img src="/img/products/icon_milan_1.png" alt="" style="margin-left: 40px;"/>
            </div>
        </div>
    </div>
    <div class="container caracteristicas">
        <div class="row">
            <div class="col-sm-6">
                <h4>Características</h4>
                <hr>
                <article>
                    <ul>
                        <li>
                            <strong>Estética</strong>
                            <p>La Reja Deacero Milán está fabricada con un diseño arquitectónico que embellece su entorno.</p>
                        </li>
                        <li>
                            <strong>Acabados seguros</strong>
                            <p>Los acabados curvos y suaves del panel aseguran que no dañen a quienes se encuentran dentro o fuera del perímetro.</p>
                        </li>
                        <li>
                            <strong>Única en el mercado</strong>
                            <p>Es la única reja con sus características, gracias a su diseño patentado.</p>
                        </li>
                    </ul>
                </article>
            </div>
            <div class="col-sm-6">
                <h4>Aplicaciones</h4>
                <hr>
                <article>
                    <ul>
                        <li>
                            Parques de diversiones.
                        </li>
                        <li>
                            Áreas comerciales.
                        </li>
                        <li>
                            Oficinas corporativas.
                        </li>
                        <li>
                            Hoteles.
                        </li>
                        <li>
                            Áreas de juegos infantiles.
                        </li>
                        <li>
                            Zonas residenciales.
                        </li>
                        <li>
                            Viñedos.
                        </li>
                        <li>
                            Estacionamientos.
                        </li>
                        <li>
                            Zoológicos.
                        </li>
                        <li>
                            Escuelas y universidades.
                        </li>
                        <li>
                            Otras.
                        </li>
                    </ul>
                </article>
            </div>
        </div>
    </div>
</div>
