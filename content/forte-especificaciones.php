<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">FORTE</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-9 caracteristicas especs">
                <div class="col-md-6 caracteristicas" style="padding: 0;margin-top: 0;">
                    <h4>ESPECIFICACIONES</h4>
                    <hr>
                </div>
                <div class="col-md-12" style="padding: 0">
                    <h5><br></h5>
                    <hr>
                    <table class="col-md-12" style="min-width: 668px;">
                        <thead>
                            <tr class="leblack">
                                <th>Diseños</th>
                                <th>Altura</th>
                                <th>Ancho</th>
                                <th colspan="2">Calibre</th>
                                <th>Altura poste</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td rowspan="4" style="background: #000;"> Plegada</td>
                                <td>m</td>
                                <td>m</td>
                                <td>cal.</td>
                                <td>mm</td>
                                <td>m</td>
                            </tr>
                            <tr>
                                <td>1.00</td>
                                <td>2.50</td>
                                <td>8</td>
                                <td>4</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>1.93</td>
                                <td>2.50</td>
                                <td>8</td>
                                <td>4</td>
                                <td>2.70</td>
                            </tr>
                            <tr>
                                <td>2.44</td>
                                <td>2.50</td>
                                <td>8</td>
                                <td>4</td>
                                <td>3.20</td>
                            </tr>
                            <tr>
                                <td rowspan="4" style="background: #000;border:solid thin #fff; color:#fff;">Plana</td>
                                <td>2.00</td>
                                <td>2.50</td>
                                <td>8</td>
                                <td>4</td>
                                <td>2.70</td>
                            </tr>
                            <tr>
                                <td>2.50</td>
                                <td>2.50</td>
                                <td>8</td>
                                <td>4</td>
                                <td>3.20</td>
                            </tr>
                            <tr>
                                <td>3.00</td>
                                <td>2.50</td>
                                <td>8</td>
                                <td>4</td>
                                <td>4.32</td>
                            </tr>
                            <tr>
                                <td>3.60</td>
                                <td>2.50</td>
                                <td>8</td>
                                <td>4</td>
                                <td>4.92</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                </div>
                <div class="col-md-12" style="padding: 0">
                    <h5><br></h5>
                    <hr>
                    <table class="col-md-12" style="min-width: 668px;">
                        <thead>
                            <tr class="leblack">
                                <th>Resistencia a la tensión del alambre</th>
                                <th>Capa de zinc</th>
                                <th colspan="2">Abertura</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td>Lb/ln2</td>
                                <td>grs/m2</td>
                                <td>plg</td>
                                <td>cm</td>
                            </tr>
                            <tr>
                                <td>70,000 - 90,000</td>
                                <td>55</td>
                                <td>1/2” x 3”</td>
                                <td>7.62 x 1.27</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 accesorios">
                    <div class="col-md-6 caracteristicas" style="padding: 0;margin-top: 40px;">
                        <h4>ACCESORIOS DE INSTALACIÓN</h4>
                        <hr>
                    </div>  
                </div>  
                <div class="col-md-12 accesorios">
                    <br>
                    <div class="col-md-3 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Poste</strong>
                        <br>
                        <small>Con remache roscado</small>
                        <br>
                        <br>
                        <img src="/img/products/poste3.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Tapa</strong>
                        <br>
                        <small>Polipropileno</small>
                        <br>
                        <br>
                        <img src="/img/products/tapa4.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Tornillos</strong>
                        <br>
                        <small>De seguridad Torx</small>
                        <br>
                        <br>
                        <img src="/img/products/tornillos2.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Grapa metálica</strong> 
                        <br>
                        <small>Galvanizada</small>
                        <br>
                        <br>
                        <img src="/img/products/grapametalica.jpg" alt=""/>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-md-12 accesorios">
                    <div class="col-md-12 accesorios">
                        <hr>
                        <br>
                        <br>
                        <strong style="margin-right: 10px;font-size: 18px">Colores </strong>
                        <img style="margin-right: 10px;"v src="/img/color-verde.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-negro.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-cafe.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-blanco.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-azul.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-amarillo.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-rojo.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-gris.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-zinc.jpg" alt=""/>
                        <br>
                        <br>
                        <br>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <br>
                <img src="/img/products/fortespec1.jpg" alt=""/>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
</div>
