<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">CLASICA</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-9 caracteristicas especs">
                <div class="col-md-6 caracteristicas" style="padding: 0;margin-top: 0;">
                    <h4>ESPECIFICACIONES</h4>
                    <hr>
                </div>
                <div class="col-md-12" style="padding: 0">
                    <h5>Panel</h5>
                    <hr>
                    <table class="col-md-12" style="min-width: 668px;">
                        <thead>
                            <tr class="leblack">
                                <th>Altura</th>
                                <th>Ancho</th>
                                <th>Abertura</th>
                                <th>Abrazaderas</th>
                                <th colspan="2">Calibre</th>
                                <th>Resistencia a la ruptura</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td>m</td>
                                <td>m</td>
                                <td>mm</td>
                                <td>#</td>
                                <td>cal.</td>
                                <td>mm</td>
                                <td>Lb/plg2</td>
                            </tr>
                            <tr>
                                <td>0.63</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                                <td>2</td>
                                <td>6</td>
                                <td>4.89</td>
                                <td>75,000 - 100,000</td>
                            </tr>
                            <tr>
                                <td>1.00</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                                <td>2</td>
                                <td>6</td>
                                <td>4.89</td>
                                <td>75,000 - 100,000</td>
                            </tr>
                            <tr>
                                <td>1.50</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                                <td>3</td>
                                <td>6</td>
                                <td>4.89</td>
                                <td>75,000 - 100,000</td>
                            </tr>
                            <tr>
                                <td>1.82</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                                <td>4</td>
                                <td>6</td>
                                <td>4.89</td>
                                <td>75,000 - 100,000</td>
                            </tr>
                            <tr>
                                <td>2.00</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                                <td>4</td>
                                <td>6</td>
                                <td>4.89</td>
                                <td>75,000 - 100,000</td>
                            </tr>
                            <tr>
                                <td>2.40</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                                <td>5</td>
                                <td>6</td>
                                <td>4.89</td>
                                <td>75,000 - 100,000</td>
                            </tr>
                            <tr>
                                <td>2.50</td>
                                <td>2.22</td>
                                <td>50 x 200</td>
                                <td>5</td>
                                <td>6</td>
                                <td>4.89</td>
                                <td>75,000 - 100,000</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                </div>
                <div class="col-md-12" style="padding: 0">
                    <h5>Poste</h5>
                    <hr>
                    <table class="col-md-12" style="min-width: 668px;">
                        <thead>
                            <tr class="leblack">
                                <th>Altura del panel</th>
                                <th>Altura del poste</th>
                                <th>Calibre</th>
                                <th>Dimensiones</th>
                                <th>Resistencia a la ruptura</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td>m</td>
                                <td>m</td>
                                <td>cal.</td>
                                <td>pig</td>
                                <td>kgf</td>
                            </tr>
                            <tr>
                                <td>0.63</td>
                                <td>1.00</td>
                                <td>16</td>
                                <td>2 1/4” x 2 1/4”</td>
                                <td>477</td>
                            </tr>
                            <tr>
                                <td>1.00</td>
                                <td>1.50</td>
                                <td>16</td>
                                <td>2 1/4” x 2 1/4”</td>
                                <td>477</td>
                            </tr>
                            <tr>
                                <td>1.50</td>
                                <td>2.00</td>
                                <td>16</td>
                                <td>2 1/4” x 2 1/4”</td>
                                <td>477</td>
                            </tr>
                            <tr>
                                <td>2.00</td>
                                <td>2.50</td>
                                <td>16</td>
                                <td>2 1/4” x 2 1/4”</td>
                                <td>477</td>
                            </tr>
                            <tr>
                                <td>2.50</td>
                                <td>3.10</td>
                                <td>16</td>
                                <td>2 1/4” x 2 1/4”</td>
                                <td>477</td>
                            </tr>
                        </tbody>
                    </table> 
                </div>
                <div class="col-md-12 accesorios">
                    <div class="col-md-6 caracteristicas" style="padding: 0;">
                        <h4>ACCESORIOS DE INSTALACIÓN</h4>
                        <hr>
                    </div>  
                </div>  
                <div class="col-md-12 accesorios">
                    <br>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Tapa clásica</strong>
                        <br>
                        <small>Polipropileno</small>
                        <br>
                        <br>
                        <img src="/img/products/tapaclasica.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Abrazadera</strong>
                        <br>
                        <small>Acero galvanizado</small>
                        <br>
                        <br>
                        <img src="/img/products/abrazadera.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Bayoneta Púa</strong>
                        <br>
                        <small>Extensión 45°</small>
                        <br>
                        <br>
                        <img src="/img/products/bayonetapua.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Bayoneta Panel</strong> 
                        <br>
                        <small>Extensión 45°</small>
                        <br>
                        <br>
                        <img src="/img/products/bayonetapanel.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Kit de base</strong>
                        <br>
                        <small>Acero galvanizado</small>
                        <br>
                        <br>
                        <img src="/img/products/kitbase.jpg" alt=""/>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc">
                        <strong class="reddot">Tuerca <br>autorrompiente</strong>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-md-12 accesorios">
                    <div class="col-md-12 accesorios">
                        <hr>
                        <br>
                        <br>
                        <strong style="margin-right: 10px;font-size: 18px">Colores </strong>
                        <img style="margin-right: 10px;"v src="/img/color-verde.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-negro.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-cafe.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-blanco.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-azul.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-amarillo.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-rojo.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-gris.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-zinc.jpg" alt=""/>
                        <br>
                        <br>
                        <br>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <br>
                <img src="/img/products/clasicaspec1.jpg" alt=""/>
            </div>
        </div>
        <br>
        <br>
        <br>
    </div>
</div>  
