<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">CLASICA</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <img src="/img/products/product_clasica.jpg" alt="" class="fillwide"/>
                <br>
                <br>
            </div>
            <div class="col-md-6 uglygrey">
                <div>
                    Es un sistema fácil de instalar que se caracteriza por tener un diseño con pliegues en forma de “V” a lo largo y ancho de cada panel para dar mayor resistencia.
                </div>
            </div>
            <div class="col-md-6" style="text-align: right">
                <img src="/img/products/iconclasic1.jpg" alt="" style="margin-left: 40px;"/>
                <img src="/img/products/iconclasic2.jpg" alt="" style="margin-left: 40px;"/>
            </div>
        </div>
    </div>
    <div class="container caracteristicas">
        <div class="row">
            <div class="col-sm-6">
                <h4>Características</h4>
                <hr>
                <article>
                    <ul>
                        <li>
                            <strong>Diseño Versátil</strong>
                            <p>La Reja Deacero Clásica tiene un diseño que puede utilizarse en diferentes tipos de proyectos.</p>
                        </li>
                        <li>
                            <strong>Seguridad</strong>
                            <p>Su pliegue en forma de “V” le da estructura y firmeza al panel. Su diseño dificulta el que sea escalable, evitando la violación del perímetro.</p>
                        </li>
                        <li>
                            <strong>Sistema de sujeción resistente</strong>
                            <p>Su sistema de sujeción está diseñado para otorgarle una alta resistencia estructural.</p>
                        </li>
                    </ul>
                </article>
            </div>
            <div class="col-sm-6">
                <h4>Aplicaciones</h4>
                <hr>
                <article>
                    <ul>
                        <li>
                            Parques de diversiones.
                        </li>
                        <li>
                            Áreas comerciales.
                        </li>
                        <li>
                            Hoteles.
                        </li>
                        <li>
                            Piscinas.
                        </li>
                        <li>
                            Áreas deportivas.
                        </li>
                        <li>
                            Áreas industriales.
                        </li>
                        <li>
                            Estacionamientos.
                        </li>
                        <li>
                            Áreas de juegos infantiles.
                        </li>
                        <li>
                            Zonas residenciales.
                        </li>
                        <li>
                            Plazas y parques.
                        </li>
                        <li>
                            Zoológicos.
                        </li>
                        <li>
                            Escuelas y universidades.
                        </li>
                        <li>
                            Otras.
                        </li>
                    </ul>
                </article>
            </div>
        </div>
    </div>
</div>
