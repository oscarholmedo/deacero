<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">MILÁN</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-9 caracteristicas especs">
                <div class="col-md-6 caracteristicas" style="padding: 0;margin-top: 0;">
                    <h4>ESPECIFICACIONES</h4>
                    <hr>
                </div>
                <div class="col-md-12" style="padding: 0">
                    <h5>Panel</h5>
                    <hr>
                    <table class="col-md-12" style="min-width: 668px;">
                        <thead>
                            <tr class="leblack">
                                <th>Altura</th>
                                <th>Longitud Panel</th>
                                <th colspan="2">Calibre Varilla</th>
                                <th>Altura poste</th>
                                <th>Profundidad cimentación</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td>m</td>
                                <td>m</td>
                                <td>cal.</td>
                                <td>mm</td>
                                <td>m</td>
                                <td>m</td>
                            </tr>
                            <tr>
                                <td>1.60</td>
                                <td>2.22</td>
                                <td>4</td>
                                <td>6</td>
                                <td>2.3</td>
                                <td>0.6</td>
                            </tr>
                            <tr>
                                <td>2.00</td>
                                <td>2.22</td>
                                <td>4</td>
                                <td>6</td>
                                <td>2.78</td>
                                <td>0.6</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                </div>
                <div class="col-md-12" style="padding: 0">
                    <h5>Poste</h5>
                    <hr>
                    <table class="col-md-12" style="min-width: 668px;">
                        <thead>
                            <tr class="leblack">
                                <th>Resistencia a la tensión del alambre</th>
                                <th>Resistencia a la ruptura</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td>m</td>
                                <td>Lb/mínimo</td>
                            </tr>
                            <tr>
                                <td>55,000 - 70,000</td>
                                <td>2,500</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
                <div class="col-md-12 accesorios">
                    <div class="col-md-6 caracteristicas" style="padding: 0;">
                        <h4>ACCESORIOS DE INSTALACIÓN</h4>
                        <hr>
                    </div>  
                </div>  
                <div class="col-md-12 accesorios">
                    <br>
                    <div class="col-md-12" style="padding: 0;">
                        <div class="col-md-3 col-sm-6 col-xs-6 acc">
                            <strong class="reddot">Grapa oval</strong>
                            <br>
                            <small>55 x 34 x 10 mm</small>
                            <br>
                            <br>
                            <img src="/img/products/grapaoval.jpg" alt=""/>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 acc">
                            <strong class="reddot">Grapa recta</strong>
                            <br>
                            <small>55 x 34 x 10 mm</small>
                            <br>
                            <br>
                            <img src="/img/products/graparecta.jpg" alt=""/>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 acc">
                            <strong class="reddot">Media grapa</strong>
                            <br>
                            <small>33 x 30 x 11 mm</small>
                            <br>
                            <br>
                            <div style="float: left;padding-right: 40px;">                                
                                <img src="/img/products/mediagrapa.jpg" alt=""/>
                            </div>
                            <small class="reddot">Pijas de acero inoxidable de 1/8” x 1” autotaladrante</small>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding: 0;">
                        <div class="col-md-3 col-sm-6 col-xs-6 acc">
                            <strong class="reddot">Poste</strong>
                            <br>
                            <small>62 x 85 mm</small>
                            <br>
                            <br>
                            <img src="/img/products/poste.jpg" alt=""/>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 acc">
                            <strong class="reddot">Tapa</strong>
                            <br>
                            <small>70 x 90 x 30 mm</small>
                            <br>
                            <br>
                            <img src="/img/products/tapa2.jpg" alt=""/>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 acc">
                            <strong class="reddot">Kit poste base</strong>
                            <small>125 x 150 x 58 mm <br>Aluminio moldeado por inyección</small>
                            <br>
                            <br>
                            <div style="float: left;padding-right: 40px;">                                
                                <img src="/img/products/kitposte.jpg" alt=""/>
                            </div>
                            <ul>
                                <li class="reddot"><small>2 opresores de acero inoxidable M 10 x 12 mm</li></small>
                                <li class="reddot"><small>1 tornillo de acero inoxidable cabeza de coche M 10 x 60 mmcon tuerca y arandela</li></small>
                                <li class="reddot"><small>4 taquetes ancla 3/8” x 3”</li></small>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 accesorios">
                    <div class="col-md-12 accesorios">
                        <hr>
                        <br>
                        <br>
                        <strong style="margin-right: 10px;font-size: 18px">Colores </strong>
                        <img style="margin-right: 10px;"v src="/img/color-verde.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-negro.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-cafe.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-blanco.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-azul.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-amarillo.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-rojo.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-gris.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-zinc.jpg" alt=""/>
                        <br>
                        <br>
                        <br>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <br>
                <img src="/img/products/milanspec1.jpg" alt=""/>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
</div>
