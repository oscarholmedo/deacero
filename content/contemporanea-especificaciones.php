<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">CONTEMPORÁNEA</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-9 caracteristicas especs">
                <div class="col-md-12 caracteristicas" style="padding: 0;margin-top: 0;">
                    <div class="col-md-6 caracteristicas" style="padding: 0;margin-top: 0;">
                        <h4>ESPECIFICACIONES</h4>
                        <hr>
                    </div>
                </div>
                <div class="col-md-6" style="padding-left:  0">
                    <h5>Panel</h5>
                    <hr>
                    <table class="col-md-6" style="width: 100%">
                        <thead>
                            <tr class="leblack" style="height:40px ">
                                <th>Diseños</th>
                                <th>Altura</th>
                                <th>Ancho</th>
                                <th>Abertura
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td rowspan="7" class="black" style="background: #000;">868 y 656</td>
                                <td>m</td>
                                <td>m</td>
                                <td>mm</td>
                            </tr>
                            <tr>
                                <td>1.00</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                            </tr>
                            <tr>
                                <td>1.20</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                            </tr>
                            <tr>
                                <td>1.60</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                            </tr>
                            <tr>
                                <td>1.80</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                            </tr>
                            <tr>
                                <td>2.00</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                            </tr>
                            <tr>
                                <td>2.40</td>
                                <td>2.50</td>
                                <td>50 x 200</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                </div>
                <div class="col-md-6" style="padding-right: 0">
                    <h5>Poste</h5>
                    <hr>
                    <table class="col-md-6" style="width: 100%;">
                        <thead>
                            <tr class="leblack">
                                <th>Altura reja</th>
                                <th>Cimentación</th>
                                <th>Altura poste</th>
                                <th>Separador, remache y tornillo de seguridad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td>m</td>
                                <td>m</td>
                                <td>m</td>
                                <td>#</td>
                            </tr>
                            <tr>
                                <td>1.00</td>
                                <td>0.5</td>
                                <td>1.58</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>1.20</td>
                                <td>0.5</td>
                                <td>1.78</td>
                                <td>4</td>
                            </tr>
                            <tr>
                                <td>1.60</td>
                                <td>0.5</td>
                                <td>2.18</td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td>1.80</td>
                                <td>0.6</td>
                                <td>2.48</td>
                                <td>6</td>
                            </tr>
                            <tr>
                                <td>2.00</td>
                                <td>0.6</td>
                                <td>2.68</td>
                                <td>6</td>
                            </tr>
                            <tr>
                                <td>2.40</td>
                                <td>0.6</td>
                                <td>3.08</td>
                                <td>7</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6" style="padding-left:  0">
                    <h5>Varilla Vertical</h5>
                    <hr>
                    <table class="col-md-6" style="width: 100%">
                        <thead>
                            <tr class="leblack" style="height:40px ">
                                <th>Diseños</th>
                                <th>Resistencia a la tensión</th>
                                <th colspan="2">Calibre</th>
                                <th>Capa de zinc</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td>&nbsp;</td>
                                <td>Lb/plg2</td>
                                <td>cal.</td>
                                <td>mm</td>
                                <td>grs/m2</td>
                            </tr>
                            <tr>
                                <td>868</td>
                                <td>75,000 - 105,000</td>
                                <td>4</td>
                                <td>6</td>
                                <td>92</td>
                            </tr>
                            <tr>
                                <td>656</td>
                                <td>75,000 - 105,000</td>
                                <td>6</td>
                                <td>5</td>
                                <td>92</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6" style="padding-right: 0">
                    <h5>Doble Varilla Horizontal</h5>
                    <hr>
                    <table class="col-md-6" style="width: 100%;">
                        <thead>
                            <tr class="leblack">
                                <th>Diseños</th>
                                <th>Resistencia <br>a la tensión</th>
                                <th colspan="2">Calibre</th>
                                <th>Capa de zinc</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="leorange">
                                <td>&nbsp;</td>
                                <td>Lb/plg2</td>
                                <td>cal.</td>
                                <td>mm</td>
                                <td>grs/m2</td>
                            </tr>
                            <tr>
                                <td>868</td>
                                <td>80,000 - 95,000</td>
                                <td>1/0</td>
                                <td>8</td>
                                <td>92</td>
                            </tr>
                            <tr>
                                <td>656</td>
                                <td>75,000 - 105,000</td>
                                <td>4</td>
                                <td>6</td>
                                <td>92</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 accesorios" style="margin-top: 40px;">
                    <div class="col-md-6 caracteristicas" style="padding: 0;">
                        <h4>ACCESORIOS DE INSTALACIÓN</h4>
                        <hr>
                    </div>  
                </div>  
                <div class="col-md-12 accesorios">
                    <br>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc" style="margin-left: 10px;">
                        <strong class="reddot">Separador</strong>
                        <br>
                        <small>Polipropileno</small>
                        <br>
                        <br>
                        <img src="/img/products/separador.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc" style="margin-left: 10px;">
                        <strong class="reddot" class="reddot">Tapa</strong>
                        <br>
                        <small>Polipropileno</small>
                        <br>
                        <br>
                        <img src="/img/products/tapa3.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc" style="margin-left: 10px;">
                        <strong class="reddot">Tornillos</strong>
                        <br>
                        <small>Torx de seguridad</small>
                        <br>
                        <br>
                        <img src="/img/products/tornillos.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc" style="margin-left: 10px;">
                        <strong class="reddot">Barra de Sujección</strong> 
                        <br>
                        <small style="white-space: nowrap;">Solera 3/16” x 1 1/2”</small>
                        <br>
                        <br>
                        <img src="/img/products/barrasujecion.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 acc" style="margin-left: 10px;">
                        <strong class="reddot">Poste</strong>
                        <small style="white-space: nowrap;">Calibre 14 <br>Dimensión 2 1/2” x 1 1/2</small>
                        <br>
                        <br>
                        <img src="/img/products/poste2.jpg" alt=""/>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-md-12 accesorios">
                    <div class="col-md-12 accesorios">
                        <hr>
                        <br>
                        <br>
                        <strong style="margin-right: 10px;font-size: 18px">Colores </strong>
                        <img style="margin-right: 10px;"v src="/img/color-verde.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-negro.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-cafe.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-blanco.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-azul.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-amarillo.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-rojo.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-gris.jpg" alt=""/>
                        <img style="margin-right: 10px;" src="/img/color-zinc.jpg" alt=""/>
                        <br>
                        <br>
                        <br>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <br>
                <img src="/img/products/clasicaspec1.jpg" alt=""/>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
