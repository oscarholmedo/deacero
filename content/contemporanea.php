<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">CONTEMPORÁNEA</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <img src="/img/products/product_contemporanea.jpg" alt="" class="fillwide"/>
                <br>
                <br>
            </div>
            <div class="col-md-6 uglygrey">
                <div>
                    Es un sistema integral de alambres soldados con diseño minimalista. Tiene gran resistencia gracias a su doble alambre de refuerzo horizontal.
                </div>
            </div>
            <div class="col-md-6" style="text-align: right">
                <img src="/img/products/icon_milan_2.png" alt="" style="margin-left: 40px;"/>
                <img src="/img/products/icon_milan_1.png" alt="" style="margin-left: 40px;"/>
            </div>
        </div>
    </div>
    <div class="container caracteristicas">
        <div class="row">
            <div class="col-sm-6">
                <h4>Características</h4>
                <hr>
                <article>
                    <ul>
                        <li>
                            <strong>Diseño minimalista y uniforme</strong>
                            <p>La Reja Deacero Contemporánea presenta un diseño de acabados planos y uniformes donde la sobriedad es la prioridad.</p>
                        </li>
                        <li>
                            <strong>Seguridad</strong>
                            <p>Los dos alambres horizontales de refuerzo, uno a cada lado del panel, le dan resistencia a impactos y recargones.</p>
                        </li>
                        <li>
                            <strong>Sistema de sujeción resistente</strong>
                            <p>Su sistema de sujeción está diseñado para otorgarle una alta resistencia estructural.</p>
                        </li>
                    </ul>
                </article>
            </div>
            <div class="col-sm-6">
                <h4>Aplicaciones</h4>
                <hr>
                <article>
                    <ul>
                        <li>
                            Parques de diversiones.
                        </li>
                        <li>
                            Áreas comerciales.
                        </li>
                        <li>
                            Oficinas corporativas.
                        </li>
                        <li>
                            Hoteles.
                        </li>
                        <li>
                            Instalaciones de gobierno.
                        </li>
                        <li>
                            Parques.
                        </li>
                        <li>
                            Estacionamientos.
                        </li>
                        <li>
                            Piscinas.
                        </li>
                        <li>
                            Escuelas y universidades.
                        </li>
                        <li>
                            Áreas deportivas.
                        </li>
                        <li>
                            Otras.
                        </li>
                    </ul>
                </article>
            </div>
        </div>
    </div>
</div>
