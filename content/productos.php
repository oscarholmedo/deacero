<div class="container-fluid greyh2">
    <div class="container">
        <h2>REJA DE ACERO</h2>
    </div>
</div>
<!-- menu de imagenes de producto-->
<div class="container-fluid fixtop2">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-6 productos-mini">
                <a href="index.php?page=reja&tipo=clasica">
                    <figure>
                        <img src="/img/products/clasica-mini.jpg" alt=""/>
                    </figure>
                    <figcaption>
                        Clásica
                    </figcaption>
                </a>
            </div>
            <div class="col-md-2 col-xs-6 productos-mini">
                <a href="index.php?page=reja&tipo=milan">
                    <figure>
                        <img src="/img/products/milan-mini.jpg" alt=""/>
                    </figure>
                    <figcaption>
                        Milán
                    </figcaption>
                </a>
            </div>
            <div class="col-md-2 col-xs-6 productos-mini">
                <a href="index.php?page=reja&tipo=florencia">
                    <figure>
                        <img src="/img/products/florencia-mini.jpg" alt=""/>
                    </figure>
                    <figcaption>
                        Florencia
                    </figcaption>
                </a>
            </div>
            <div class="col-md-2 col-xs-6 productos-mini">
                <a href="index.php?page=reja&tipo=contemporanea">
                    <figure>
                        <img src="/img/products/contemporanea-mini.jpg" alt=""/>
                    </figure>
                    <figcaption>
                        Contemporánea
                    </figcaption>
                </a>
            </div>
            <div class="col-md-2 col-xs-6 productos-mini">
                <a href="index.php?page=reja&tipo=forte">
                    <figure>
                        <img src="/img/products/forte-mini.jpg" alt=""/>
                    </figure>
                    <figcaption>
                        Forte
                    </figcaption>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12" style="padding: 0;">
                <div class="col-md-12 uglygrey" >
                    <div style="padding: 30px;">
                        Las Rejas Deacero son un sistema integral desarrollado con una tecnología que busca combinar la estética, seguridad, funcionalidad y resistencia para brindar a los usuarios el mejor sistema de rejas disponible.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin de menu de imagenes de producto-->
<!--caracteristicas generales-->
<div class="container caracteristicas">
    <div class="row">
        <div class="col-sm-5">
            <h4>Características</h4>
            <hr>
            <article>
                El sistema de Reja Deacero está compuesto por:
                <br>
                Paneles, postes, abrazaderas, grapas, puertas, portones y accesorios especiales. 
                <br>
                Todos los componentes metálicos de la Reja Deacero son galvanizados y recubiertos con pintura de poliéster termoendurecido, lo que le brinda mayor duración contra la oxidación.
            </article>
        </div>
        <div class="col-sm-5 pull-right">
            <h4>Ventajas</h4>
            <hr>
            <article>
                <ul>
                    <li>Instalación sencilla. No requiere soldadura.</li>
                    <li>Ahorro en comparación con la herrería tradicional.</li>
                    <li>Armonía y visibilidad con el entorno.</li>
                    <li>Durabilidad y bajos costos de mantenimiento.</li>
                    <li>Aplica para certificación LEED.</li>
                </ul>
            </article>            
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            <h4>Acabados</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <article>
                Acabados libres de componentes orgánicos volatiles (VOC). Todos los sistemas en cumplimiento de las normas: ASTM - 510, A641, B117, NOM B - 365, B507 y G -154-04.
            </article>            
        </div>
        <div class="col-sm-6">
            <article>
                <strong>Estándar:</strong>
                Ofrece una protección con 3 capas anticorrosivas; zinc, circonio y poliéster termo - endurecido. Durabilidad de 1,000 horas en prueba de cámara salina.
                <br>
                <br>
                <figure>
                    <img src="/img/estandarDesgin.jpg" alt="" style="width:70%;max-width: 380px;height: auto;"/>
                </figure>
                <br>
            </article>            
        </div>
        <div class="col-sm-6">
            <article>
                <strong>Marina:</strong>
                Durabilidad extrema gracias a sus 4 capas anticorrosivas; las mismas tres del acabado estándar mas un primer protector.
                <br>
                <br>
                <figure>
                    <img src="/img/marinaDesign.jpg" alt="" style="width: 70%;max-width: 380px;height: auto;"/>
                </figure>
                <br>
            </article>            
        </div>
        <div class="col-sm-6">
            <article>
                <table class="aplicaciones">
                    <thead>
                        <tr>
                            <th><span>APLICACIONES</span></th>
                            <th><span>CLÁSICA </span></th>
                            <th><span>MILÁN </span></th>
                            <th><span>FLORENCIA </span></th>
                            <th><span>CONTEMPORÁNEA </span></th>
                            <th><span>FORTE </span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span>Parque de Diversiones</span> </td>
                            <td> ••• </td>
                            <td> ••• </td>
                            <td> •• </td>
                            <td> •• </td>
                            <td> •</td>
                        </tr>
                        <tr>
                            <td> <span>Áreas Comerciales</span></td>
                            <td> ••• </td>
                            <td> •• </td>
                            <td> • </td>
                            <td> ••• </td>
                            <td> •</td>
                        </tr>
                        <tr>
                            <td> <span>Oficinas Corporativas</span></td>
                            <td> •• </td>
                            <td> ••• </td>
                            <td> ••• </td>
                            <td> •• </td>
                            <td> •</td>
                        </tr>
                        <tr>
                            <td> <span>Instalaciones de Gobierno</span></td>
                            <td> •• </td>
                            <td> • </td>
                            <td> •• </td>
                            <td> ••• </td>
                            <td> •••</td>
                        </tr>
                        <tr>
                            <td> <span>Hoteles</span></td>
                            <td> •• </td>
                            <td> ••• </td>
                            <td> ••• </td>
                            <td> •• </td>
                            <td> •</td>
                        </tr>
                        <tr>
                            <td> <span>Áreas Industriales</span></td>
                            <td> ••• </td>
                            <td> •</td>
                            <td> •</td>
                            <td> ••• </td>
                            <td> ••</td>
                        </tr>
                        <tr>
                            <td> <span>Zonas Militares</span></td>
                            <td> ••</td>
                            <td> •</td>
                            <td> •</td>
                            <td> ••• </td>
                            <td> •••</td>
                        </tr>
                        <tr>
                            <td> <span>Estacionamientos</span></td>
                            <td> •••</td>
                            <td> ••</td>
                            <td> •</td>
                            <td> ••• </td>
                            <td> ••</td>
                        </tr>
                        <tr>
                            <td> <span>Parques</span></td>
                            <td>•••</td>
                            <td>••</td>
                            <td>•••</td>
                            <td>•</td>
                            <td>•</td>
                        </tr>
                        <tr>
                            <td> <span>Juegos para Niños</span></td>
                            <td>••</td>
                            <td>•••</td>
                            <td>••</td>
                            <td>•••</td>
                            <td>•</td>
                        </tr>
                        <tr>
                            <td> <span>Piscinas</span></td>
                            <td>•••</td>
                            <td>••</td>
                            <td>•</td>
                            <td>•••</td>
                            <td>•</td>
                        </tr>
                        <tr>
                            <td> <span>Prisiones</span></td>
                            <td>•</td>
                            <td>•</td>
                            <td>•</td>
                            <td>•••</td>
                            <td>•••</td>
                        </tr>
                        <tr>
                            <td> <span>Zonas Residenciales</span></td>
                            <td>••</td>
                            <td>•••</td>
                            <td>•••</td>
                            <td>•</td>
                            <td>•</td>
                        </tr>
                        <tr>
                            <td> <span>Comercios</span></td>
                            <td>•••</td>
                            <td>••</td>
                            <td>•</td>
                            <td>•••</td>
                            <td>•</td>
                        </tr>
                        <tr>
                            <td> <span>Escuelas y Universidades</span></td>
                            <td>•••</td>
                            <td>••</td>
                            <td>••</td>
                            <td>•••</td>
                            <td>•</td>
                        </tr>
                        <tr>
                            <td> <span>Áreas Deportivas</span></td>
                            <td>•••</td>
                            <td>•</td>
                            <td>•</td>
                            <td>•••</td>
                            <td>••</td>
                        </tr>
                        <tr>
                            <td> <span>Viñedos</span></td>
                            <td>•••</td>
                            <td>•••</td>
                            <td>••</td>
                            <td>•</td>
                            <td>•</td>
                        </tr>
                        <tr>
                            <td> <span>Depósito y Almacenamiento</span></td>
                            <td>••</td>
                            <td>•</td>
                            <td>•</td>
                            <td>•••</td>
                            <td>•••</td>
                        </tr>
                        <tr>
                            <td> <span>Zoológicos</span></td>
                            <td>••</td>
                            <td>•</td>
                            <td>•</td>
                            <td>•••</td>
                            <td>•••</td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <span>••• Aplicación ideal</span>
                    <span>•• Aplicación recomendada</span>
                    <span>• Aplicación aceptable</span>
                </div>
            </article>            
        </div>

        <div class="col-md-6 stickers">
            <small>
                <div class="row">
                    <div class="col-md-6">
                        <article>
                            <table>
                                <tr>
                                    <td>
                                        <img src="/img/rejamarina.png"/>
                                    </td>
                                    <td style="text-align: left;">
                                        <strong>Acabado Reja Marina:</strong> Capa de pintura epóxica conocida como primer zinc rich que ofrece una duración extrema para instalaciones cercanas al mar.
                                    </td>
                                </tr>
                            </table>
                        </article>            
                    </div>
                    <div class="col-md-6">
                        <article>
                            <table>
                                <tr>
                                    <td>
                                        <img src="/img/abrazadera.png"/>
                                    </td>
                                    <td style="text-align: left;">
                                        <strong>Abrazadera:</strong> Cuenta con bisagra, tuerca y tornillo de acero inoxidable. Para áreas de mayor seguridad se recomienda la tuerca autorrompiente para evitar el robo.
                                    </td>
                                </tr>
                            </table>
                        </article>            
                    </div>
                    <div class="col-md-6">
                        <article>
                            <table>
                                <tr>
                                    <td>
                                        <img src="/img/corrosion.png"/>
                                    </td>
                                    <td style="text-align: left;">
                                        <strong>Protección adicional a la corrosión:</strong> La capa de circonio que protege a la Reja Deacero es única en el mercado. Brinda mayor protección a la oxidación junto con las capas de zinc y poliéster termoendurecido.
                                    </td>
                                </tr>
                            </table>
                        </article>            
                    </div>
                    <div class="col-md-6">
                        <article>
                            <table>
                                <tr>
                                    <td>
                                        <img src="/img/leed.png"/>
                                    </td>
                                    <td style="text-align: left;">
                                        <strong>Certificación LEED:</strong> Proceso de fabricación es ecológico y con tecnología de punta para crear un producto sustentable, todo el acero que se utiliza es 100% reciclado y reciclable. Reja Deacero califica como producto para la acreditación LEED.
                                    </td>
                                </tr>
                            </table>
                        </article>            
                    </div>
                    <div class="col-md-6">
                        <article>
                            <table>
                                <tr>
                                    <td>
                                        <img src="/img/global.png"/>
                                    </td>
                                    <td style="text-align: left;">
                                        <strong>Globalidad:</strong> Los sistemas de cercos de Reja Deacero ya son utilizados en proyectos de prestigio en diversos países de América, desde Estados Unidos hasta Chile.
                                    </td>
                                </tr>
                            </table>
                        </article>            
                    </div>
                    <div class="col-md-6">
                        <article>
                            <table>
                                <tr>
                                    <td>
                                        <img src="/img/locinox.png"/>
                                    </td>
                                    <td style="text-align: left;">
                                        <strong>Herrajes Locinox:</strong> Todos los sistemas de herrajes que se utilizan con Reja Deacero son de la marca Locinox, aliado de Deacero en ofrecer cercos perimetrales de alta calidad.
                                    </td>
                                </tr>
                            </table>
                        </article>            
                    </div>
                </div>
            </small>                
        </div>  
    </div>
</div>
