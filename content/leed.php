<div class="container-fluid greyh2">
    <div class="container">
        <h2>LEED Y SUSTENTABILIDAD</h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container caracteristicas">
        <br>
        <div class="row">
            <div class="col-sm-5">
                <h4>INICIATIVA SUSTENTABLE</h4>
                <hr>
                <article>
                    Grupo Deacero y todas las marcas de sus líneas de productos visualizan a la sustentabilidad como el eje rector de sus operaciones, generando valor en lo social, económico y ambiental.
                    <br>
                    <br>
                    Deacero esta acreditado por la World Steel Association como miembro activo de su programa Climate Action, gracias a su estricto control de consumo de energía y agua en la fabricación de sus productos, así como en la emisión de partículas en el medio ambiente.
                    <br>
                    <br>
                    Reja Deacero es un producto fabricado con un acabado libre de componentes orgánicos volátiles (VOC).
                    <br>
                    <br>
                </article>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-6" style="padding-right: 0;">
                    <img src="/img/leed1.jpg" alt="" style="width: 100%;"/>
                    <br>
                    <br>
                    <br>
                </div>
                <div class="col-sm-6" style="padding-right: 0;">
                    <img src="/img/leed2.jpg" alt="" style="width: 100%;"/>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
            <div class="col-sm-12">
                <img src="/img/leedBar.jpg" alt="" style="width:100%;"/>
                <br>
                <br>
                <br>
            </div>
            <div class="col-sm-5">
                <img src="/img/leed3.jpg" alt="" style="width: 100%;"/>
                <br>
                <br>
            </div>
            <div class="col-sm-7">
                <h4>CERTIFICACIÓN LEED Y REJA DEACERO</h4>
                <hr>
                <article>
                    Deacero es miembro activo del United States Green Building Council, entidad rectora del programa de certificación LEED para proyectos de construcción sustentables. 
                    <br>
                    <br>
                    La certificación LEED promueve el uso de productos renovables o fabricados con insumos reciclados en diversos proyectos de construcción.
                    <br>
                    <br>
                    Reja Deacero, como sistema integral de reja, aplica a ciertos créditos necesarios para lograr la certificación LEED de aquellas obras y empresas con ésta meta en mente. 
                    <br>
                    <br>
                    En específico Reja Deacero aplica a los siguientes apartados LEED: MR 4.1 y 4.2 – Créditos relacionados al uso de productos hechos con materiales reciclados y con la capacidad de ser reciclables.
                    <br>
                    <br>
                    De esta manera Reja Deacero tiene un potencial de aplicación hasta a cuatro créditos LEED, en la búsqueda de la certificación de su proyecto.
                    <br>
                    <br>
                    <img src="/img/leed4.jpg" alt="" style="height: 100px;width: auto;"/>
                    <br>
                    <br>
                </article>
            </div>
        </div>
    </div>
</div>
