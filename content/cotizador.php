<div class="container-fluid greyh2">
    <div class="container">
        <h2>COTIZADOR</h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container caracteristicas">
        <div class="row">
            <div class="col-md-6">
                <h4>COTIZA EN LÍNEA</h4>
                <hr>
            </div>
            <div class="col-md-12">
                <article>
                    Siga los siguientes pasos sencillos para cotizar su proyecto con Reja Deacero: 
                    <br>
                    <br>
                    1. Descargue el archivo Excel para iniciar su cotización. <a download  href="documentacion/cotizador-reja-de-acero-almasa.xlsx">Enlace de descarga.</a>
                    <br>
                    <br>
                    2. Identifique su tipo de proyecto y de acuerdo a esto el tipo de reja que más se adecua a su requerimiento.
                    <br>
                    <br>
                    3. Elija la pestaña del archivo con la especificación a cotizar tanto en tipo de reja como altura (H).
                    <br>
                    <img src="img/garantia-1.png" alt=""/>
                    <br>
                    <br>
                    4. Ingrese el No. De ML que requiere cotizar para obtener las cantidades necesarias.                    
                    <br>
                    <img src="img/garantia-2.png" alt=""/>
                    <br>
                    <br>
                    5. Contacte a un asesor o coordinador para conocer el valor de su proyecto y para obtener toda la asesoría técnica necesaria.
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </article>
            </div>
        </div>
    </div>
</div>
