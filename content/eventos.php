<div class="container-fluid greyh2">
    <div class="container">
        <h2>EVENTOS</h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container caracteristicas">
        <div class="row">
            <div class="col-sm-6">
                <h4>EVENTOS RECIENTES</h4>
                <hr>
                <article>
                    2013
                    <br>
                    <strong>EXPOMETÁLICA</strong>
                    <br>
                    Medellín, Colombia
                    <br>
                    <br>
                    2014
                    <br>
                    <strong>EXPOCAMACOL</strong>
                    <br>
                    Medellín, Colombia
                    <br>
                    <br>
                    <strong>ASOCRETO</strong>
                    <br>
                    Cartagena, Colombia
                    <br>
                    <br>
                    2015
                    <br>
                    <strong>EXPOCONSTRUCCIÓN</strong>
                    <br>
                    Bogotá, Colombia
                    <br>
                    <br>
                    <strong>EXPOINFRAESTRUCTURA</strong>
                    <br>
                    Medellín, Colombia
                    <br>
                    <br>
                    <strong>CHARLA SOCIEDAD COLOMBIANA DE ARQUITECTOS SECCIONAL ATLÁNTICO</strong>
                    <br>
                    Barranquilla, Colombia.
                    <br>
                    <br>
                    <strong>EXPODEFENSA</strong>
                </article>
            </div>
            <div class="col-sm-6">
                <h4>PRÓXIMOS EVENTOS</h4>
                <hr>
                <article>
                    2015
                    <br>
                    <strong>CHARLA SOCIEDAD COLOMBIANA DE ARQUITECTOS SECCIONAL ANTIOQUIA</strong>
                </article>
            </div>
            <div class="col-sm-12" >
                <div class="row galeria">
                    <div class="col-md-3">
                        <img src="/img/events/asocreto2.jpg" alt=""/>
                    </div>
                    <div class="col-md-3">
                        <img src="/img/events/expocamacol2.jpg" alt=""/>
                    </div>
                    <div class="col-md-3">
                        <img src="/img/events/charlaSca3.jpg" alt=""/>
                    </div>
                    <div class="col-md-3">
                        <img src="/img/events/expocamacol3.jpg" alt=""/>
                    </div>
                </div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>
