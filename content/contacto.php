<div class="container-fluid greyh2">
    <div class="container">
        <h2>CONTACTO</h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container caracteristicas">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-5" style="margin:auto -15px;">
                    <div class="col-sm-12" style="margin:auto -15px;">
                        <h4>ZONA ANTIOQUIA</h4>
                        <hr>
                        <address>
                            <p>
                                CATALINA ECHEVERRI
                                <br>
                                <i class="fa fa-phone"></i> 3202 719 192
                                <br>
                                <a href="mailto:catalina.echeverri@almasa.com.co" style="color:#000;"><i class="fa fa-envelope"></i> catalina.echeverri<span>@</span>almasa.com.co</a>
                            </p>
                        </address>
                    </div>
                    <div class="col-sm-12" style="margin:auto -15px;">
                        <h4>ZONA ATLÁNTICO</h4>
                        <hr>
                        <address>
                            <p>
                                MARLON AGUDELO
                                <br>
                                <i class="fa fa-phone"></i> 3123 972 959
                                <br>
                                <a href="mailto:marlon.agudelo@almasa.com.co" style="color:#000;"><i class="fa fa-envelope"></i> marlon.agudelo<span>@</span>almasa.com.co</a>
                            </p>
                        </address>
                    </div>
                    <div class="col-sm-12" style="margin:auto -15px;">
                        <h4>ZONA BOGOTÁ</h4>
                        <hr>
                        <address>
                            <p>
                                KARIN DELUQUE
                                <br>
                                <i class="fa fa-phone"></i> 3212 138 310
                                <br>
                                <a href="mailto:karin.deluque@almasa.com.co" style="color:#000;"><i class="fa fa-envelope"></i> karin.deluque<span>@</span>almasa.com.co</a>
                            </p>
                        </address>
                    </div>
                    <div class="col-sm-12" style="margin:auto -15px;">
                        <h4>DIRECTOR DE LÍNEA ARQUITECTURA</h4>
                        <hr>
                        <address>
                            <p>
                                KATHERINE TORRES
                                <br>
                                <i class="fa fa-phone"></i> 3202 719 192
                                <br>
                                <a href="mailto:katherine.torres@almasa.com.co" style="color:#000;"><i class="fa fa-envelope"></i> katherine.torres<span>@</span>almasa.com.co</a>
                            </p>
                        </address>
                    </div>
                </div>
                <div class="col-sm-6" style="background: #f0f0f0;">
                </div>
            </div>
        </div>
    </div>
</div>
