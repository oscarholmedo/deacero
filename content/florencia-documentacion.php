<div class="container-fluid greyh2">
    <div class="container">
        <h2>PRODUCTOS / <span class="hprod">FLORENCIA</span></h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container caracteristicas documentacion">
        <div class="row">
            <div class="col-md-6">
                <h4>Documentación</h4>
                <hr>
                <br>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table proDocTbl">
                    <thead>
                        <tr>
                            <th>PANEL</th>
                            <th>Archivo DWG</th>
                            <th>Archivo PDF</th>
                            <th>Especificaciones</th>
                            <th>Descripción Técnica</th>
                            <th>Tabla de Calidad</th>
                            <th>Folleto</th>
                            <th>Manual de Instalación</th>
                            <th>Guía de Mantenimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="grey">0.80</td>
                            <td class="grey"><a download href="documentacion/<?= $tipo ?>-0_8.dwg"><img alt="" src="/img/dwg-24.png" class="img-responsive"></a> </td>
                            <td class="grey"><a download href="documentacion/<?= $tipo ?>-0_8.pdf"><img alt="" src="/img/pdf.png" class="img-responsive"></a> </td>
                            <td rowspan="5" class="grey"><a download href="documentacion/<?= $tipo ?>-especificaciones.pdf"><img alt="" src="/img/pdf.png" class="img-responsive"></a> </td>
                            <td rowspan="5" class="grey"><a download href="documentacion/<?= $tipo ?>-descripcion.pdf"><img alt="" src="/img/pdf.png" class="img-responsive"></a> </td>
                            <td rowspan="5" class="grey"><a download href="documentacion/rejas-tabla-de-calidad.pdf"><img alt="" src="/img/pdf.png" class="img-responsive"></a> </td>
                            <td rowspan="5" class="grey"><a download href="documentacion/rejas-folleto.pdf"><img alt="" src="/img/pdf.png" class="img-responsive"></a> </td>
                            <td rowspan="5" class="grey"><a download href="documentacion/<?= $tipo ?>-manual-de-instalacion.pdf"><img alt="" src="/img/pdf.png" class="img-responsive"></a> </td>
                            <td rowspan="5" class="grey"><a download href="documentacion/rejas-guia-de-mantenimiento.pdf"><img alt="" src="/img/pdf.png" class="img-responsive"></a> </td>
                        </tr>
                        <tr>
                            <td class="grey">1.20</td>
                            <td class="grey"><a download href="documentacion/<?= $tipo ?>-1_2.dwg"><img alt="" src="/img/dwg-24.png" class="img-responsive"></a> </td>
                            <td class="grey"><a download href="documentacion/<?= $tipo ?>-1_2.pdf"><img alt="" src="/img/pdf.png" class="img-responsive"></a> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <br>
                <br>
                <br>
                <div style="width: 100%; text-align: center">                    
                    <iframe width="420" height="315" src="https://www.youtube.com/embed/v6Y4Ce5aeXQ?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>
