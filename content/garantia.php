<div class="container-fluid greyh2">
    <div class="container">
        <h2>GARANTÍA</h2>
    </div>
</div>
<div class="container-fluid fixtop2">
    <div class="container caracteristicas">
        <div class="row">
            <div class="col-md-5">
                <h4>GARANTÍA DE FABRICACIÓN</h4>
                <hr>
            </div>
            <div class="col-md-12">
                <article>
                    La Reja Deacero Galvanizada y con Poliéster Termo - Endurecido ofrece una garantía de 10 años contra defectos de fabricación, corrosión y desgaste de su capa protectora, en sus paneles, postes y abrazaderas. 
                    <br>
                    <br>
                    <a download class="btn btn-default btn-sm" href="documentacion/garantia-reja-de-acero-v2-abr-15.pdf"><img src="/img/pdf.png" alt=""/> Descargar garantía</a>
                    <br>
                    <br>
                    Es indispensable cumplir con la Guía de Mantenimiento para hacer válida la garantía.
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </article>
            </div>
        </div>
    </div>
</div>
