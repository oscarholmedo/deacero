<?php
$page = (isset($_GET['page']) && $_GET['page'] != '') ? $_GET['page'] : 'landingpage';
$tipo = (isset($_GET['tipo']) && $_GET['tipo'] != '') ? $_GET['tipo'] : '';
$seccion = (isset($_GET['seccion']) && $_GET['seccion'] != '') ? "-$_GET[seccion]" : '';
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Reja de Acero</title>
        <meta name="description" content="Seguridad con buen gusto">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/apple-touch-icon.png" />
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/css/main.css">
        <script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:400,600,700|Homenaje' rel='stylesheet' type='text/css'>
    </head>
    <body class="<?= $page ?>">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include "includes/menu.php" ?>
        <?php include "content/$page.php" ?>
        <?php include "includes/footer.php" ?>

        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times fa-2x"></i></button>
                </div>
                <div class="modal-content">              
                </div>
            </div>
        </div>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');</script>
        <script>
            jQuery('.gallery img').on('click', function (e) {
                var img = '<img src="' + jQuery(this).attr('src') + '"/>';
                jQuery('.modal-content').html(img);
                jQuery('.modal').modal('show');
            });
            //route
            var friendly_urls = true;
            if (friendly_urls) {
                jQuery('a').each(function (e) {
                    var ugly_url = jQuery(this).attr('href');
                    var pretify = true;
                    switch (ugly_url.split('.').pop()) {
                        //excluir documentos de descarga
                        case 'xlsx':
                        case 'pdf':
                        case 'dwg':
                            pretify = false;
                            sef = "http://<?= $_SERVER['SERVER_NAME'] ?>/" + ugly_url;
                            break;
                        default:
                            break;
                    }
                    if (ugly_url.split(':').shift() == 'mailto') {
                        pretify = false;
                        sef = ugly_url;
                    }
                    if (ugly_url.substring(0, 1) == '#') {
                        pretify = false;
                        sef = ugly_url;
                    }
                    if (pretify) {
                        var urlParams = ugly_url.split(/[?&]/).slice(1).map(function (paramPair) {
                            return paramPair.split(/=(.+)?/).slice(0, 2);
                        }).reduce(function (obj, pairArray) {
                            obj[pairArray[0]] = pairArray[1];
                            return obj;
                        }, {});
                        var sef = '/';
                        if (urlParams.page != undefined) {
                            sef += urlParams.page;
                        }
                        if (urlParams.tipo != undefined) {
                            sef += '/' + urlParams.tipo;
                        }
                        if (urlParams.seccion != undefined) {
                            sef += '/' + urlParams.seccion;
                        }
                    }
                    jQuery(this).attr('href', sef);
                }
                );
            }

            //galeria
            jQuery('.gallery .row div').height(jQuery('.gallery .row div:first-child').width()+30);
            jQuery(window).resize(function () {
                jQuery('.gallery .row div').height(jQuery('.gallery .row div:first-child').width()+30);
            });
        </script>
    </body>
</html>

