<div class="container-fluid footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="/img/almasalogo.jpg" alt=""/>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2">
                <span class="w">Almasa.</span> Todos los derechos reservados.
            </div>
            <div class="col-md-2">
                <span class="w">Teléfonos: </span> <span class="nb">1.270 0777 • 3212 138310</span>
            </div>
            <div class="col-md-2">
                <span class="w">almasa.com.co</span>
                <a href="mailto:rejadeacero@almasa.com.co" style="color:#999;">rejadeacero<span>@</span>almasa.com.co</a>
            </div>
        </div>
    </div>
</div>