<div class="container-fluid mainmenu ">
    <nav class="navbar container">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-bars fa-2x"></i>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="/img/logo.jpg" alt="" style="margin-left: -5px;"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href=""><span>Reja Deacero</span></a></li>
                    <li>
                        <a href="index.php?page=productos"><span>Productos</span></a>
                        <ul>
                            <li><a href="index.php?page=reja&tipo=clasica">Clásica</a></li>
                            <li><a href="index.php?page=reja&tipo=milan">Milán</a></li>
                            <li><a href="index.php?page=reja&tipo=florencia">Florencia</a></li>
                            <li><a href="index.php?page=reja&tipo=contemporanea">Contemporánea</a></li>
                            <li><a href="index.php?page=reja&tipo=forte">Forte</a></li>
                        </ul>
                    </li>
                    <li><a href="index.php?page=leed"><span>Leed y Sustentabilidad</span></a></li>
                    <li><a href="index.php?page=garantia"><span>Garantía</span></a></li>
                    <li><a href="index.php?page=cotizador"><span>Cotizador</span></a></li>
                    <li><a href="index.php?page=eventos"><span>Eventos</span></a></li>
                    <li><a href="index.php?page=almasa"><span>Almasa</span></a></li>
                    <li><a href="index.php?page=contacto"><span>Contacto</span></a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>
<div class="fixtop"></div>
