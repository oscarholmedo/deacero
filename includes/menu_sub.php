<div class="container-fluid submenu">
    <nav class="navbar container">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-2" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-bars fa-2x"></i>
                </button>
                <!--
                <a class="navbar-brand" href="#">
                    <img src="/img/logo.jpg" alt="" style="margin-left: -5px;"/>
                </a>
                -->
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse row" id="navbar-collapse-2">
                <ul class="nav navbar-nav col-md-12">
                    <li class="col-sm-3 <?= !$seccion ? 'active' : '' ?>"><a href="<?=$base_dir?>index.php?page=<?= $page ?>&tipo=<?= $tipo ?>"><span>Descripción</span></a></li>
                    <li class="col-sm-3 <?= $seccion == "-especificaciones" ? 'active' : '' ?>"><a href="<?=$base_dir?>index.php?page=<?= $page ?>&tipo=<?= $tipo ?>&seccion=especificaciones"><span>Especificaciones</span></a></li>
                    <li class="col-sm-3 <?= $seccion == "-galeria" ? 'active' : '' ?>"><a href="<?=$base_dir?>index.php?page=<?= $page ?>&tipo=<?= $tipo ?>&seccion=galeria"><span>Galería de imágenes</span></a></li>
                    <li class="col-sm-3 <?= $seccion == "-documentacion" ? 'active' : '' ?>"><a href="<?=$base_dir?>index.php?page=<?= $page ?>&tipo=<?= $tipo ?>&seccion=documentacion"><span>Documentación</span></a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>